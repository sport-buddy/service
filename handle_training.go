package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"net/http"
	"strconv"
)

func (s *serverImpl) addTrainingHandler(c echo.Context) error {
	var training Training
	if err := c.Bind(&training); err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	log.Info(training.MeetDate)
	id, err := getIdFromContext(c)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	training.Owner = id
	training, err = s.trainingManager.AddTraining(training)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, training)
}

func (s *serverImpl) getTrainingHandler(c echo.Context) error {
	paramId := c.Param("id")
	id, err := strconv.ParseInt(paramId, 10, 64)
	if err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	t, err := s.trainingManager.GetTraining(id)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, t)
}

func (s *serverImpl) deleteTrainingHandler(c echo.Context) error {
	paramId := c.Param("id")
	id, err := strconv.ParseInt(paramId, 10, 64)
	if err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	idUser, err := getIdFromContext(c)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	if err := s.trainingManager.DeleteTraining(id, idUser); err != nil {
		return err
	}
	return c.NoContent(http.StatusOK)
}

func (s *serverImpl) removeUserFromTrainingHandler(c echo.Context) error {
	paramId := c.Param("id")
	id, err := strconv.ParseInt(paramId, 10, 64)
	if err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	idUser, err := getIdFromContext(c)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	if err = s.trainingManager.RemoveUserFromTraining(id, idUser); err != nil {
		return err
	}
	return c.NoContent(http.StatusOK)
}

func (s *serverImpl) searchTrainingsHandler(c echo.Context) error {
	userId, err := getIdFromContext(c)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	var filter TrainingFilter
	if err := c.Bind(&filter); err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, fmt.Sprintf("Invalid filter parameters: %s", err.Error()))
	}
	trainings, err := s.trainingManager.SearchTrainings(userId, filter)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, trainings)
}

func (s *serverImpl) getUserTrainingsHandler(c echo.Context) error {
	userId, err := getIdFromContext(c)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	trainings, err := s.trainingManager.GetUsersTrainings(userId)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, trainings)
}

func (s *serverImpl) addUserToTraining(c echo.Context) error {
	idUser, err := getIdFromContext(c)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	var req MemberTraining
	err = c.Bind(&req)
	if err != nil {
		log.Errorf("failed to unmarshal request: %s", err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	req.IdUser = idUser

	err = s.trainingManager.AddUserToTraining(req)
	if err != nil {
		return err
	}
	return c.NoContent(http.StatusOK)
}

func (s *serverImpl) UpdateTrainingHandler(c echo.Context) error {
	training := Training{}
	err := c.Bind(&training)
	if err != nil {
		return c.String(http.StatusInternalServerError, "Invalid training parameters")
	}
	id, err := getIdFromContext(c)
	if err != nil {
		return c.String(http.StatusInternalServerError, fmt.Sprintf("Failed to update training: %s", err.Error()))
	}
	if err := validateTraining(training); err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	if err := s.trainingManager.UpdateTraining(training, id); err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, training)
}

func getIdFromContext(c echo.Context) (int64, error) {
	id, ok := c.Get("id_user").(string)
	if !ok {
		log.Error("incorrect id ", c.Get("id_user"))
		return 0, c.JSON(http.StatusInternalServerError, "Failed to parse id from token")
	}
	numId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.Error(err)
		return 0, c.JSON(http.StatusInternalServerError, "Failed to parse id from token")
	}
	return numId, nil
}
