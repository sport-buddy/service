package main

import (
	"crypto/rand"
	"encoding/base32"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/gommon/log"
	"net/http"
	"net/mail"
	"regexp"
	"time"
)

var (
	userNamePattern     = regexp.MustCompile(`^[a-zA-Z]{1}[a-zA-Z0-9_.]{0,30}$`)
	userPwdPattern      = regexp.MustCompile(`^[a-zA-Z0-9_\-@#$%^&\.*]{8,64}$`)
	sexPattern          = regexp.MustCompile(`^male|female$`)
	namePattern         = regexp.MustCompile(`^[A-zА-я]*$`)
	trainingKindPattern = regexp.MustCompile(`^personal|group$`)
)

type (
	UserManager interface {
		Start()

		Authenticate(authProps AuthProps) (LoginResponse, *http.Cookie, error)
		Refresh(tknId string, device string) (LoginResponse, *http.Cookie, error)
		Logout(tokenId string) error
		AddUser(authProps AuthProps) (LoginResponse, *http.Cookie, error)

		GetProfile(id int64) (Profile, error)
		UpdateProfile(profile Profile) error
	}
	userManager struct {
		persistent Persistent
	}
)

func NewUserManager(pers Persistent) UserManager {
	return &userManager{persistent: pers}
}

func (um *userManager) Start() {
	um.WatchExpired()
}

func (um *userManager) Authenticate(authProps AuthProps) (LoginResponse, *http.Cookie, error) {
	user, err := um.persistent.Authenticate(authProps)
	if err != nil {
		return LoginResponse{}, nil, err
	}
	return um.addSession(user.IdUser, user.Username, authProps.device)
}

func (um *userManager) AddUser(authProps AuthProps) (LoginResponse, *http.Cookie, error) {
	user, err := um.persistent.AddUser(authProps)
	if err != nil {
		return LoginResponse{}, nil, err
	}
	return um.addSession(user.IdUser, user.Username, authProps.device)
}

func (um *userManager) Logout(tokenId string) error {
	return um.persistent.RemoveSession(tokenId)
}

func (um *userManager) Refresh(tknId string, device string) (LoginResponse, *http.Cookie, error) {
	newTknId, err := generateTokenID()
	if err != nil {
		return LoginResponse{}, nil, err
	}
	newRefreshToken := Token{
		Token:     newTknId,
		LoginTime: time.Now().UTC(),
		Expires:   time.Now().UTC().Add(RefreshTokenExpiration),
		Device:    device,
	}
	authProps, err := um.persistent.RefreshSession(tknId, newRefreshToken)
	if err != nil {
		return LoginResponse{}, nil, err
	}
	accessTkn, err := getAccessToken(authProps.IdUser, authProps.Username)
	if err != nil {
		return LoginResponse{}, nil, err
	}

	resp := LoginResponse{
		IdUser:      accessTkn.IdUser,
		Username:    accessTkn.Username,
		AccessToken: accessTkn.Token,
	}

	cookie := &http.Cookie{
		Name:     refreshToken,
		Value:    newRefreshToken.Token,
		Expires:  newRefreshToken.Expires,
		Path:     "/auth",
		HttpOnly: true,
	}

	return resp, cookie, nil
}

func (um *userManager) GetProfile(id int64) (Profile, error) {
	return um.persistent.GetProfile(id)
}

func (um *userManager) UpdateProfile(profile Profile) error {
	err := validateUserProfile(profile)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	if profile.DateOfBirth != "" {
		profile.Birthday, err = time.Parse("2006-01-02", profile.DateOfBirth)
		if err != nil {
			log.Error(err.Error())
			return err
		}
	}
	return um.persistent.UpdateProfile(profile)
}

func (um *userManager) addSession(id int64, username, device string) (LoginResponse, *http.Cookie, error) {
	accessTkn, err := getAccessToken(id, username)
	if err != nil {
		return LoginResponse{}, nil, err
	}
	refreshTkn, err := getRefreshToken(id, username)
	if err != nil {
		return LoginResponse{}, nil, err
	}
	refreshTkn.Device = device
	if err = um.persistent.AddSession(refreshTkn); err != nil {
		return LoginResponse{}, nil, err
	}

	resp := LoginResponse{
		IdUser:      accessTkn.IdUser,
		Username:    accessTkn.Username,
		AccessToken: accessTkn.Token,
	}

	cookie := &http.Cookie{
		Name:     refreshToken,
		Value:    refreshTkn.Token,
		Expires:  refreshTkn.Expires,
		Path:     "/auth",
		HttpOnly: true,
	}

	return resp, cookie, nil
}

func (um *userManager) WatchExpired() {
	go func() {
		for {
			um.persistent.RemoveExpired()
			time.Sleep(time.Minute)
		}
	}()
	return
}

func getAccessToken(id int64, username string) (Token, error) {
	claims := jwt.StandardClaims{
		Id:        fmt.Sprintf("%d", id),
		ExpiresAt: time.Now().UTC().Add(time.Hour).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &claims)

	accessTokenId, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Errorf("failed to create access token: %s", err.Error())
		return Token{}, fmt.Errorf("failed to create access token: %s", err.Error())
	}
	return Token{
		IdUser:    id,
		Username:  username,
		Token:     accessTokenId,
		Expires:   time.Unix(claims.ExpiresAt, 0),
		LoginTime: time.Now().UTC(),
	}, nil
}

func getRefreshToken(id int64, username string) (Token, error) {
	refreshTokenId, err := generateTokenID()
	if err != nil {
		log.Errorf("failed to create refresh token: %s", err.Error())
		return Token{}, fmt.Errorf("failed to create refresh token: %s", err.Error())
	}

	return Token{
		IdUser:    id,
		Username:  username,
		Token:     refreshTokenId,
		LoginTime: time.Now().UTC(),
		Expires:   time.Now().UTC().Add(RefreshTokenExpiration),
	}, nil
}

func generateTokenID() (string, error) {
	size := 30
	b := make([]byte, size)
	_, err := rand.Read(b)
	if err != nil {
		log.Error("failed to create token: ", err)
		return "", err
	}

	return base32.StdEncoding.EncodeToString(b), nil
}

func validateUserProfile(profile Profile) error {
	if profile.Weight < 0 || profile.Weight > 500 {
		return errors.New("weight cannot be less than 0 or more than 500")
	}
	if profile.Height < 0 || profile.Height > 300 {
		return errors.New("height cannot be less than 0 or more than 300")
	}
	if profile.Sex != "" {
		if !isStringValid(profile.Sex, sexPattern) {
			return errors.New(getErrorTextByPattern(sexPattern))
		}
	}
	if profile.IdLevel < 0 || profile.IdLevel > 3 {
		return errors.New("level id can be 0, 1, 2, 3 only")
	}
	if profile.Name != "" {
		if !isStringValid(profile.Name, namePattern) {
			return errors.New(getErrorTextByPattern(namePattern))
		}
	}
	if profile.SecondName != "" {
		if !isStringValid(profile.SecondName, namePattern) {
			return errors.New(getErrorTextByPattern(namePattern))
		}
	}
	if profile.DateOfBirth != "" {
		_, err := time.Parse("2006-06-02", profile.DateOfBirth)
		if err != nil {
			return errors.New("date have to be in format YYYY-MM-DD")
		}
	}

	if profile.Email != "" {
		_, err := mail.ParseAddress(profile.Email)
		if err != nil {
			return err
		}
	}
	return nil
}

func isStringValid(value string, pattern *regexp.Regexp) bool {
	return pattern.Match([]byte(value))
}

func getErrorTextByPattern(pattern *regexp.Regexp) string {
	switch pattern {
	case userNamePattern:
		return "user name length should be between 1 and 31, start with latin letter and it may contain only latin letters, underscores and dots"
	case userPwdPattern:
		return "password length should be between 8 and 64 and it may contain only latin letters, number and the following characters: _-@#$%%^&.*"
	case sexPattern:
		return "sex value can be male or female only"
	case namePattern:
		return "name may contain only russian and latin letters"
	case trainingKindPattern:
		return "training kind may be only personal or group"
	default:
		return "provided value is invalid"
	}
}
