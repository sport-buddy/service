# README #

###Supported systems
    * Windows:  ./bin/sport_buddy.exe
    * Linux:    ./bin/sport_buddy
    * MacOS:    ./bin/sport_buddy_darwin


```
Usage of ./bin/sport_buddy_darwin:
  -db_name string
        database name to create and use for app (default "sb_1")
  -port string
        port to run the app on (default "8080")
  -ps_password string
        password to login to PostgreSQL (default "1")
  -ps_user string
        username to login to PostgreSQL (default "postgres")

```

