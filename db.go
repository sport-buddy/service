package main

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/pkger"
	"os"
	"path/filepath"
	"time"

	"github.com/labstack/gommon/log"
	"github.com/markbates/pkger"
	_ "github.com/markbates/pkger/pkging/mem"
	gPostgres "gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os/exec"
)

var migrationsDir = getMigrationsPath()

func BootstrapDatabase(user, pwd, dbname string) (*gorm.DB, error) {
	db, err := openDb(user, pwd, dbname)
	if err != nil {
		err = createDb(user, pwd, dbname)
		if err != nil {
			log.Error(err)
			return nil, err
		}
		db, err = openDb(user, pwd, dbname)
		if err != nil {
			log.Error(err)
			return nil, err
		}
	}
	database, err := db.DB()
	if err != nil {
		log.Error(err)
		return nil, err
	}
	driver, err := postgres.WithInstance(database, &postgres.Config{})
	if err != nil {
		log.Error(err)
		return nil, err
	}
	pkger.Include(migrationsDir)
	p := filepath.ToSlash(migrationsDir)
	m, err := migrate.NewWithDatabaseInstance(fmt.Sprintf("pkger://%s", p), dbname, driver)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	err = m.Up()
	log.Error(err)
	if errors.Is(err, migrate.ErrNoChange) {
		return db, nil
	}
	return db, err
}

func openDb(pguser, pgpwd, dbname string) (*gorm.DB, error) {
	dsn := "user=" + pguser + " password=" + pgpwd + " dbname=" + dbname + " sslmode=disable"
	db, err := gorm.Open(gPostgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return db, nil
}

func createDb(pguser, pgpwd, dbname string) error {
	os.Setenv("PGPASSWORD", pgpwd)
	cmd := exec.Command("createdb", "-p", "5432", "-h", "127.0.0.1", "-U", pguser, "-e", dbname)
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		log.Error(err)
		return err
	}
	return nil
}

func Shutdown(pguser, pgpwd, dbname string) error {
	file, err := os.Create("dump.sh")
	if err != nil {
		return err
	}
	file.Write([]byte(
		`#!/bin/sh
while getopts u:p:d:f: flag
do
    case "${flag}" in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        d) dbname=${OPTARG};;
        f) filename=${OPTARG};;
    esac
done
export PGPASSWORD='$(password)'
pg_dump -U $username $dbname > $filename
`))
	date := time.Now().Format("02.01.06-15:04:05")
	filename := fmt.Sprintf("./db/dumps/%s_db.sql", date)
	_, err = os.Create(filename)
	if err != nil {
		return err
	}
	_ = exec.Command("chmod", "+x", file.Name()).Run()
	_, err = exec.Command(file.Name(), "-f", filename, "-p", pgpwd, "-u", pguser, "-d", dbname).Output()
	if err != nil {
		return err
	}
	log.Info("successfully dump database")
	return nil
}
