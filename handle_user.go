package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"net/http"
	"strconv"
)

func (s *serverImpl) getUserProfileHandler(c echo.Context) error {
	reqId := c.Param("id")
	id, err := strconv.ParseInt(reqId, 10, 64)
	if err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, "invalid user id")
	}
	profile, err := s.userManager.GetProfile(id)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, profile)
}

func (s *serverImpl) updateUserProfileHandler(c echo.Context) error {
	var profile Profile
	err := c.Bind(&profile)
	if err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	tknId := c.Get("id_user").(string)

	if tknId != strconv.FormatInt(profile.IdUser, 10) {
		return c.JSON(http.StatusForbidden, "cannot update another user's profile")
	}
	err = s.userManager.UpdateProfile(profile)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, profile)
}
