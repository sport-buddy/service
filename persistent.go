package main

import (
	"errors"
	"fmt"
	"github.com/labstack/gommon/log"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"strings"
	"time"
)

type (
	Persistent interface {
		Authenticate(authProps AuthProps) (AuthProps, error)
		AddUser(authProps AuthProps) (AuthProps, error)
		UpdateProfile(profile Profile) error
		GetProfile(id int64) (Profile, error)

		AddSession(token Token) error
		RemoveSession(tknId string) error
		RemoveExpired()
		RefreshSession(oldToken string, newToken Token) (AuthProps, error)

		AddTraining(training Training) (Training, error)
		DeleteTraining(idTraining int64) error
		GetTraining(id int64) (Training, error)
		SearchTrainings(idUser int64, query string, m map[string]interface{}) ([]Training, error)
		UpdateTraining(training Training) error

		GetUserTrainings(idUser int64) ([]Training, error)
		AddUserToTraining(req MemberTraining) error
		RemoveUserFromTraining(req MemberTraining) error
	}
	psql struct {
		db *gorm.DB
	}
)

func NewPsqlPersistent(dbConnection *gorm.DB) Persistent {
	return &psql{db: dbConnection}
}

func (p *psql) Authenticate(authProps AuthProps) (AuthProps, error) {
	var usr AuthProps
	res := p.db.Table(`users`).Where(`username=?`, authProps.Username).Find(&usr)
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return AuthProps{}, err
	}
	match := struct {
		Pswmatch bool
	}{}

	res = p.db.Table(`user_auth_info`).Select(`(password = crypt(?, password)) AS pswmatch`, authProps.Password).
		Where(`id_user=?`, usr.IdUser).Find(&match)
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return AuthProps{}, err
	}
	if !match.Pswmatch {
		err := fmt.Errorf("invalid creds")
		log.Error(err.Error())
		return AuthProps{}, err
	}

	return usr, nil
}

func (p *psql) AddUser(authProps AuthProps) (AuthProps, error) {
	tx := p.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Error("rolling back")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Error(err)
		return AuthProps{}, err
	}

	res := tx.Table(`users`).Select(`username`).Create(&authProps)
	if err := res.Error; err != nil {
		tx.Rollback()
		log.Error(err)
		return AuthProps{}, err
	}

	userInfo := Profile{IdUser: authProps.IdUser, Name: authProps.Username}
	res = tx.Table(`user_info`).Select("id_user", "name").Create(&userInfo)
	if err := res.Error; err != nil {
		tx.Rollback()
		log.Error(err)
		return AuthProps{}, err
	}

	res = tx.Exec(`INSERT INTO user_auth_info  (id_user, password) VALUES (?, crypt(?, gen_salt('bf', 8)));`, authProps.IdUser, authProps.Password)
	if err := res.Error; err != nil || res.RowsAffected == 0 {
		tx.Rollback()
		log.Error(err)
		return AuthProps{}, err
	}

	if err := tx.Commit().Error; err != nil {
		log.Error(err)
		return AuthProps{}, err
	}
	return authProps, nil
}

func (p *psql) UpdateProfile(profile Profile) error {
	tx := p.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Error("rolling back")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Error(err.Error())
		return err
	}

	res := tx.Table(`user_info`).Where(`id_user=?`, profile.IdUser).Updates(&profile)
	if err := res.Error; err != nil {
		log.Error(err)
		tx.Rollback()
		return err
	}
	if len(profile.PersonSports) > 0 {
		var sportIds []int64

		var st []sport
		var sp []personSports

		res = tx.Table(`sports`).Where(`sport_type IN ?`, profile.PersonSports).Find(&st)
		if err := res.Error; err != nil {
			log.Error(err.Error())
			tx.Rollback()
			return err
		}

		for _, s := range st {
			sp = append(sp, personSports{
				IdUser:  profile.IdUser,
				IdSport: s.IdSport,
			})
		}
		res = tx.Table(`person_sports`).Clauses(clause.OnConflict{DoNothing: true}).Create(&sp)
		if err := res.Error; err != nil {
			tx.Rollback()
			log.Errorf("error occurred while updating person's sports: %s", err.Error())
			return err
		}

		for _, s := range sp {
			sportIds = append(sportIds, s.IdSport)
		}

		res = tx.Table(`person_sports`).Where(`id_user=? AND id_sport NOT IN ?`, profile.IdUser, sportIds).Delete(&personSports{})
		if err := res.Error; err != nil {
			tx.Rollback()
			err := fmt.Errorf("error occurred while updating person's sports: %s", err.Error())
			log.Error(err.Error())
			return err
		}
	} else {
		res = tx.Table(`person_sports`).Where(`id_user=?`, profile.IdUser).Delete(&personSports{})
		if err := res.Error; err != nil {
			tx.Rollback()
			err := fmt.Errorf("error occurred while updating person's sports: %s", res.Error)
			log.Error(err.Error())
			return err
		}
	}

	if len(profile.Location) > 0 {
		var ids []int64
		for i := range profile.Location {
			var id int64
			res = tx.Table(`locations`).Select(`id_location`).Where(`location=?`, profile.Location[i]).Take(&id)
			if err := res.Error; err != nil {
				log.Error(err.Error())
				return err
			}
			ins := struct {
				IdUser     int64
				IdLocation int64
			}{
				profile.IdUser,
				id,
			}
			res = tx.Table(`person_locations`).Clauses(clause.OnConflict{DoNothing: true}).Create(&ins)
			if err := res.Error; err != nil {
				log.Error(err.Error())
				return err
			}
			ids = append(ids, id)
		}

		res = tx.Table(`person_locations`).Where(`id_user=? AND id_location NOT IN ?`, profile.IdUser, ids).Delete(&personSports{})
		if err := res.Error; err != nil {
			tx.Rollback()
			err := fmt.Errorf("error occurred while updating person's location: %s", res.Error)
			log.Error(err.Error())
			return err
		}
	} else {
		res = tx.Table(`person_locations`).Where(`id_user=?`, profile.IdUser).Delete(&personSports{})
		if err := res.Error; err != nil {
			tx.Rollback()
			err := fmt.Errorf("error occurred while updating person's location: %s", res.Error)
			log.Error(err.Error())
			return err
		}
	}

	if err := tx.Commit().Error; err != nil {
		log.Error(err.Error())
		return err
	}
	return nil
}

func (p *psql) AddSession(token Token) error {
	res := p.db.Table(`sessions`).Select("id_user").Where("id_user=? AND device=?", token.IdUser, token.Device).First(new(Token))
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		res = p.db.Table(`sessions`).Select("id_user", "login_time", "token", "expires", "device").Create(&token)
		if err := res.Error; err != nil || res.RowsAffected == 0 {
			err := fmt.Errorf("failed to save token to database: %s", err.Error())
			log.Error(err)
			return err
		}
	}
	res = p.db.Table(`sessions`).Select("id_user", "login_time", "token", "expires", "device").Where("id_user=? AND device=?", token.IdUser, token.Device).Updates(&token)
	if err := res.Error; err != nil {
		err := fmt.Errorf("failed to add session: %s", err.Error())
		log.Error(err.Error())
		return err
	}
	return nil
}

func (p *psql) RemoveSession(tknId string) error {
	res := p.db.Table(`sessions`).Where("token=?", tknId).Delete(&Token{})
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return err
	}
	return nil
}

func (p *psql) RemoveExpired() {
	res := p.db.Table(`sessions`).Where(`expires<now()`).Delete(&Token{})
	if res.Error != nil {
		log.Error(res.Error.Error())
	}
}

func (p *psql) RefreshSession(oldToken string, newToken Token) (AuthProps, error) {
	var authProps AuthProps
	p.RemoveExpired()
	res := p.db.Table(`sessions`).Where("token=?", oldToken).Updates(newToken).Joins("JOIN users ON sessions.id_user=user.id_user AND token=?", newToken.Token).Select("id_user").First(&authProps)
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return AuthProps{}, err
	}
	return authProps, nil
}

func (p *psql) GetProfile(idUser int64) (Profile, error) {
	var profile Profile
	profile.IdUser = idUser

	res := p.db.Table(`user_info`).
		Select(`user_info.id_user, id_level, name, second_name, sex, height, weight, email, about, birthday, array_agg(s.sport_type) as sports, array_agg(l.location) as locations, 
extract(year from age(now(), user_info.birthday)) as age`).
		Where(`user_info.id_user=?`, profile.IdUser).
		Joins(`
LEFT JOIN person_sports ps ON user_info.id_user = ps.id_user
LEFT JOIN sports s ON s.id_sport = ps.id_sport
LEFT JOIN person_locations pl ON user_info.id_user = pl.id_user
LEFT JOIN locations l ON pl.id_location = l.id_location`).
		Group(`user_info.id_user`).
		First(&profile)
	if err := res.Error; err != nil {
		log.Error(err)
		return Profile{}, err
	}

	if profile.Sports != "{NULL}" {
		withoutBrackets := strings.Trim(profile.Sports, "{}")
		profile.PersonSports = strings.Split(withoutBrackets, ",")
	}

	if profile.Locations != "{NULL}" {
		withoutBrackets := strings.Trim(profile.Locations, "{}")
		profile.Location = strings.Split(withoutBrackets, ",")
	}

	nullTime := time.Time{}
	if profile.Birthday != nullTime {
		profile.DateOfBirth = profile.Birthday.Format("2006-01-02")
	}
	return profile, nil
}

func (p *psql) getSport(s string) (sport, error) {
	resSport := sport{SportType: s}
	res := p.db.Table(`sports`).Where(`sport_type=?`, resSport.SportType).First(&resSport)
	if err := res.Error; err == gorm.ErrRecordNotFound {
		log.Errorf("no sport with name %s", s)
		return sport{}, fmt.Errorf("no sport with name %s", s)
	} else if err != nil {
		log.Errorf("failed to get sport: %s", err.Error())
		return sport{}, fmt.Errorf("failed to get sport: %s", err)
	}
	return resSport, nil
}

func (p *psql) AddTraining(training Training) (Training, error) {
	s, err := p.getSport(training.Sport)
	if err != nil {
		return Training{}, err
	}
	training.IdSport = s.IdSport
	tx := p.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Error("rolling back")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Error(err.Error())
		return Training{}, err
	}

	res := tx.Table(`group_training`).Select(`meet_date`, `location`, `id_sport`, `id_level`, `duration`, `comment`, `fee`).Create(&training)
	if err := res.Error; err != nil {
		tx.Rollback()
		log.Error(err.Error())
		return Training{}, fmt.Errorf("failed to add group training: %s", err.Error())
	}
	if res.RowsAffected == 0 {
		tx.Rollback()
		err := fmt.Errorf("failed to add training")
		log.Error(err.Error())
		return Training{}, err
	}
	mt := MemberTraining{
		IdUser:        training.Owner,
		IdTraining:    training.IdTraining,
		TrainingOwner: true,
	}
	res = tx.Table(`member_training`).Create(&mt)
	if err := res.Error; err != nil || res.RowsAffected == 0 {
		tx.Rollback()
		log.Error(err)
		return training, fmt.Errorf("failed to add group training: %s", err)
	}
	training.ParticipantsIds = []int64{training.Owner}

	if err := tx.Commit().Error; err != nil {
		log.Error(err.Error())
		return Training{}, err
	}

	training.TrainingDuration = time.Duration(training.Duration).String()

	return training, nil
}

func (p *psql) GetTraining(id int64) (Training, error) {
	result := Training{}
	res := p.db.Table(`group_training`).Select(`group_training, group_training.id_training, group_training.location, group_training.meet_date, group_training.duration, s.sport_type as sport, group_training.id_level, group_training.comment, group_training.fee`).Joins(`LEFT JOIN sports s on group_training.id_sport = s.id_sport`).Where(`id_training=?`, id).Find(&result)
	if err := res.Error; err == gorm.ErrRecordNotFound {
		err := fmt.Errorf("no training with id %d", id)
		log.Error(err.Error())
		return Training{}, err
	} else if err != nil {
		log.Error(err.Error())
		return Training{}, err
	}
	var mts []MemberTraining
	res = p.db.Table(`member_training`).Where(`id_training=?`, result.IdTraining).Find(&mts)
	if err := res.Error; err == gorm.ErrRecordNotFound {
		err := fmt.Errorf("no members in training with id %d", result.IdTraining)
		log.Error(err.Error())
		return Training{}, err
	} else if err != nil {
		log.Error(res.Error)
		return Training{}, err
	}
	for _, mt := range mts {
		result.ParticipantsIds = append(result.ParticipantsIds, mt.IdUser)
		if mt.TrainingOwner {
			result.Owner = mt.IdUser
		}
	}
	result.TrainingDuration = time.Duration(result.Duration).String()
	return result, nil
}

func (p *psql) SearchTrainings(idUser int64, query string, m map[string]interface{}) ([]Training, error) {
	var result []Training
	var filtered []Training
	var res *gorm.DB

	subQuery := p.db.Table(`group_training`).
		Select(`group_training.id_training, group_training.location, group_training.meet_date, group_training.duration, s.sport_type as sport, group_training.id_level, group_training.comment, group_training.fee`).
		Joins(`LEFT JOIN member_training mt ON group_training.id_training = mt.id_training`).
		Joins(`LEFT JOIN sports s on group_training.id_sport = s.id_sport`).
		Where(`meet_date > now() AND id_user!=?`, idUser)

	if len(m) > 0 {
		res = p.db.Table(`(?) as t`, subQuery).Where(query, m).Order(`meet_date DESC`).Find(&result)
		if err := res.Error; err != nil {
			log.Error(err.Error())
			return nil, err
		}
	} else {
		res = p.db.Table(`(?) as t`, subQuery).Order(`meet_date DESC`).Find(&result)
		if err := res.Error; err != nil {
			log.Error(err.Error())
			return nil, err
		}
	}
LOOP:
	for i, t := range result {
		var mts []MemberTraining
		res = p.db.Table(`member_training`).Where(`id_training=?`, t.IdTraining).Find(&mts)
		if err := res.Error; err != nil {
			log.Error(err.Error())
			return nil, err
		}
		for _, mt := range mts {
			if mt.IdUser == idUser {
				continue LOOP
			}
			result[i].ParticipantsIds = append(result[i].ParticipantsIds, mt.IdUser)
			if mt.TrainingOwner {
				result[i].Owner = mt.IdUser
			}
		}
		result[i].TrainingDuration = time.Duration(result[i].Duration).String()
		filtered = append(filtered, result[i])
	}
	return filtered, nil
}

func (p *psql) GetUserTrainings(idUser int64) ([]Training, error) {
	var result []Training
	subQuery := p.db.Table(`group_training`).Select(`group_training.id_training, meet_date, location, sport_type as sport, id_level, fee, duration, comment`).
		Joins(`LEFT JOIN member_training mt ON group_training.id_training = mt.id_training 
LEFT JOIN sports s ON group_training.id_sport = s.id_sport`).Where(`id_user=?`, idUser)
	res := p.db.Table(`(?) as gt`, subQuery).Select(`member_training.id_training, meet_date, location, sport, id_level, fee, duration, comment, member_training.id_user as owner `).
		Joins(`LEFT JOIN member_training ON gt.id_training=member_training.id_training`).Where(`training_owner=true`).Find(&result)
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return nil, err
	}

	for i := range result {
		var mts []MemberTraining
		res = p.db.Table(`member_training`).Where(`id_training=?`, result[i].IdTraining).Find(&mts)
		if err := res.Error; err != nil {
			log.Error(err.Error())
			return nil, err
		}
		for _, mt := range mts {
			result[i].ParticipantsIds = append(result[i].ParticipantsIds, mt.IdUser)
		}
		log.Infof("duration: %s", result[i].Duration)
		result[i].TrainingDuration = time.Duration(result[i].Duration).String()
	}
	return result, nil
}

func (p *psql) AddUserToTraining(req MemberTraining) error {
	res := p.db.Table(`member_training`).Create(&req)
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return err
	}
	return nil
}

func (p *psql) RemoveUserFromTraining(req MemberTraining) error {
	res := p.db.Table(`member_training`).Where("id_user=? AND id_training=?", req.IdUser, req.IdTraining).Delete(&req)
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return err
	}
	return nil
}

func (p *psql) DeleteTraining(idTraining int64) error {
	var t = Training{
		IdTraining: idTraining,
	}
	res := p.db.Table(`group_training`).Delete(&t)
	if err := res.Error; err != nil {
		log.Error(err.Error())
		return err
	}
	return nil
}

func (p *psql) UpdateTraining(training Training) error {
	if d, err := time.ParseDuration(training.TrainingDuration); err == nil {
		training.Duration = Duration(d)
	}
	s, err := p.getSport(training.Sport)
	if err != nil {
		return err
	}
	training.IdSport = s.IdSport
	tx := p.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Error("rolling back")
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		log.Error(err.Error())
		return err
	}

	res := tx.Table(`group_training`).Select(`meet_date`,
		`location`, `id_sport`, `id_level`, `duration`, `comment`, `fee`).Updates(&training)
	if err := res.Error; err != nil || res.RowsAffected == 0 {
		tx.Rollback()
		log.Error(err.Error())
		return err
	}
	var mt []MemberTraining

	trainingOwner := false
	for _, u := range training.ParticipantsIds {
		if u == training.Owner {
			trainingOwner = true
		}
		mt = append(mt, MemberTraining{
			IdUser:        u,
			IdTraining:    training.IdTraining,
			TrainingOwner: u == training.Owner,
		})
	}
	if training.Owner == 0 || !trainingOwner {
		tx.Rollback()
		err := errors.New("no owner provided or trying to delete owner from participants")
		log.Error(err.Error())
		return err
	}
	res = tx.Table(`member_training`).Clauses(clause.OnConflict{DoNothing: true}).Create(&mt)
	if err := res.Error; err != nil {
		tx.Rollback()
		log.Error(err.Error())
		return fmt.Errorf("failed to add group training: %s", err)
	}
	res = tx.Table(`member_training`).Where(`id_training=? AND id_user NOT IN ? AND training_owner!=?`,
		training.IdTraining, training.ParticipantsIds, trainingOwner).Delete(&MemberTraining{})
	if err = res.Error; err != nil {
		tx.Rollback()
		log.Error(err.Error())
		return fmt.Errorf("failed to add group training: %s", err)
	}

	if err := tx.Commit().Error; err != nil {
		log.Error(err.Error())
		return err
	}

	training.TrainingDuration = time.Duration(training.Duration).String()

	return nil
}
