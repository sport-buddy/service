package main

import (
	"database/sql/driver"
	"fmt"
	"time"
)

type (
	AuthProps struct {
		IdUser   int64  `json:"id_user" gorm:"primaryKey"`
		Username string `json:"username"`
		Password string `json:"password"`

		device string
	}
	Profile struct {
		IdUser int64 `json:"id_user" gorm:"primaryKey"`
		// First name of a user
		Name string `json:"name" example:"Андрей"`
		// Second name of a user
		SecondName string `json:"second_name" example:"Попов"`
		// Sex of a user
		Sex string `json:"sex" example:"male"`
		// Height of a user
		Height int64 `json:"height" example:"180"`
		// Weight of a user
		Weight int64 `json:"weight" example:"80"`
		// todo: maybe remove?
		Email string `json:"email" example:"andrey@gmail.com"`
		// todo: remove
		IdLevel int64 `json:"id_level" example:"1"`
		// todo: remove
		Location []string `json:"location" gorm:"-" example:"Nevskiy"`
		// Date of birth of a user
		DateOfBirth string `json:"date_of_birth,omitempty" gorm:"-" example:"2001-01-01"`
		// Agr of a user
		Age int64 `json:"age,omitempty" example:"20"`
		// Description of a user
		About string `json:"about" example:"Я люблю бегать"`
		// Kinds of sports of a user
		PersonSports []string  `json:"person_sports" gorm:"-" example:"волейбол"`
		Birthday     time.Time `gorm:"birthday" json:"-"`
		Sports       string    `json:"-"`
		Locations    string    `json:"-"`
	} // @name UserProfile

	Token struct {
		IdUser    int64     `json:"id_user"`
		Username  string    `json:"username"`
		Token     string    `json:"token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Mzk3NTA4ODksImp0aSI6Ijk2ODUzMTM1MTIifQ.23KTpkYThTWo00w1yzGchj-jDg8bL8AacrAPx2c1XSk"`
		LoginTime time.Time `json:"login_time" example:"2021-11-29T19:40:42.198851Z"`
		Expires   time.Time `json:"expires" example:"2021-11-29T19:55:42.198851Z"`
		Device    string    `gorm:"device" json:"-"`
	}

	LoginResponse struct {
		// Token to access protected pages
		IdUser      int64  `json:"id_user" example:"9338554"`
		Username    string `json:"username" example:"andrey"`
		AccessToken string `json:"access_token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzYyMjUxNDcsImp0aSI6IjkxMzQ1NzQ5NzUifQ.hiQUF6DNwoOcYsBvo1-aRVEQShzRMvGYReHWKg6QY4I"`
	} // @name LoginResponse

	Training struct {
		IdTraining       int64     `json:"id_training,omitempty" gorm:"primaryKey" example:"001"`
		Owner            int64     `json:"owner" example:"002"`
		MeetDate         time.Time `json:"meet_date" example:"2021-12-23T19:00:00Z"`
		TrainingDuration string    `json:"training_duration" example:"1h0m0s"`
		Location         string    `json:"location" example:"Nevskiy"`
		Sport            string    `json:"sport" example:"football"`
		IdLevel          int64     `json:"id_level" example:"1"`
		Comment          string    `json:"comment" example:"Comment"`
		Fee              int64     `json:"fee" example:"500"`
		ParticipantsIds  []int64   `json:"participants_ids" gorm:"-" example:"002"`
		Duration         Duration  `json:"-" gorm:"duration"`
		IdSport          int64     `json:"-"`
	}

	TrainingFilter struct {
		Location []string `json:"location,omitempty" example:"Nevskiy"`
		Sport    []string `json:"sport,omitempty" example:"tennis"`
		IdLevel  []int64  `json:"id_level,omitempty" example:"2"`
	}

	Duration time.Duration

	sport struct {
		IdSport   int64 `gorm:"primaryKey"`
		SportType string
	}

	personSports struct {
		IdUser  int64
		IdSport int64
	}

	MemberTraining struct {
		IdUser        int64 `json:"id_from" gorm:"id_user"`
		IdTraining    int64 `json:"id_training"`
		TrainingOwner bool  `json:"-" gorm:"training_owner"`
	}
)

// Value converts Duration to a primitive value ready to written to a database.
func (d Duration) Value() (driver.Value, error) {
	return driver.Value(int64(d)), nil
}

// Scan reads a Duration value from database driver type.
func (d *Duration) Scan(raw interface{}) error {
	switch v := raw.(type) {
	case int64:
		*d = Duration(v)
	case nil:
		*d = Duration(0)
	default:
		return fmt.Errorf("cannot sql.Scan() strfmt.Duration from: %#v", v)
	}
	return nil
}
