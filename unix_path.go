// +build !windows

package main

import (
	"os"
	"path/filepath"
)

func getMigrationsPath() string {
	p, err := os.Getwd()
	if err != nil {
		return ""
	}
	return filepath.Join(p, "migrations_dump")
}
