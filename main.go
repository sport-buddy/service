package main

import (
	"flag"
	"log"
)

func main() {
	psUser := flag.String("ps_user", "postgres", "username to login to PostgreSQL")
	psPwd := flag.String("ps_password", "1", "password to login to PostgreSQL")
	dbName := flag.String("db_name", "sb_1", "database name to create and use for app")
	port := flag.String("port", "3000", "port to run the app on")
	flag.Parse()

	database, err := BootstrapDatabase(*psUser, *psPwd, *dbName)
	if err != nil {
		log.Fatal(err)
	}

	pers := NewPsqlPersistent(database)
	trgMgr := NewTrainingManager(pers)
	usrMgr := NewUserManager(pers)
	usrMgr.Start()

	server := NewServer(*port, usrMgr, trgMgr)
	server.Start()
}
