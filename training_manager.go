package main

import (
	"errors"
	"fmt"
	"github.com/labstack/gommon/log"
	"strings"
	"time"
)

type (
	TrainingManager interface {
		AddTraining(training Training) (Training, error)
		GetTraining(id int64) (Training, error)
		SearchTrainings(idUser int64, filter TrainingFilter) ([]Training, error)
		RemoveUserFromTraining(trainingId, idUser int64) error
		DeleteTraining(idTraining, idUser int64) error
		UpdateTraining(training Training, idUser int64) error

		GetUsersTrainings(id int64) ([]Training, error)
		AddUserToTraining(req MemberTraining) error
	}

	trainingMgr struct {
		persistent Persistent
	}
)

func NewTrainingManager(pers Persistent) TrainingManager {
	return &trainingMgr{persistent: pers}
}

func (t *trainingMgr) AddTraining(training Training) (Training, error) {
	if err := validateTraining(training); err != nil {
		log.Error(err.Error())
		return Training{}, err
	}
	d, err := time.ParseDuration(training.TrainingDuration)
	if err == nil {
		training.Duration = Duration(d)
	}

	return t.persistent.AddTraining(training)
}

func (t *trainingMgr) GetTraining(id int64) (Training, error) {
	return t.persistent.GetTraining(id)
}

func (t *trainingMgr) GetUserTrainings(id int64) ([]Training, error) {
	return t.persistent.GetUserTrainings(id)
}

func (t *trainingMgr) SearchTrainings(idUser int64, filter TrainingFilter) ([]Training, error) {
	q, m := BuildMapAndQueryForGroupTraining(filter)
	return t.persistent.SearchTrainings(idUser, q, m)
}

func (t *trainingMgr) RemoveUserFromTraining(trainingId, idUser int64) error {
	return t.persistent.RemoveUserFromTraining(MemberTraining{IdTraining: trainingId, IdUser: idUser})
}

func (t *trainingMgr) DeleteTraining(idTraining, idUser int64) error {
	training, err := t.GetTraining(idTraining)
	if err != nil {
		return err
	}
	if training.Owner != idUser {
		err := fmt.Errorf("cannot delete training %d owned by another user", idTraining)
		log.Error(err.Error())
		return err
	}
	return t.persistent.DeleteTraining(idTraining)
}

func (t *trainingMgr) GetUsersTrainings(id int64) ([]Training, error) {
	return t.persistent.GetUserTrainings(id)
}

func (t *trainingMgr) AddUserToTraining(req MemberTraining) error {
	return t.persistent.AddUserToTraining(req)
}

func (t *trainingMgr) UpdateTraining(training Training, idUser int64) error {
	trainingFromDb, err := t.persistent.GetTraining(training.IdTraining)
	if err != nil {
		return err
	}
	if trainingFromDb.Owner != idUser {
		return errors.New("cannot update training owned by another user")
	}
	return t.persistent.UpdateTraining(training)
}

func validateTraining(training Training) error {
	nullTime := time.Time{}
	if training.Owner == 0 || len(training.ParticipantsIds) == 0 || training.MeetDate == nullTime || training.Sport == "" || training.Location == "" {
		return errors.New("training owner, participants ids, kind, meet date, sport, location are required")
	}

	if training.TrainingDuration != "" {
		_, err := time.ParseDuration(training.TrainingDuration)
		if err != nil {
			return err
		}
	}

	if training.IdLevel < 0 || training.IdLevel > 3 {
		return errors.New("level id can be 0, 1, 2, 3 only")
	}
	if training.MeetDate.Before(time.Now()) {
		return errors.New("meet date may not be earlier than current")
	}
	if training.Fee < 0 {
		return errors.New("fee may be positive")
	}
	return nil
}

func BuildMapAndQueryForGroupTraining(filter TrainingFilter) (string, map[string]interface{}) {
	queryParts := make([]string, 0)
	resMap := make(map[string]interface{})
	if filter.Location != nil {
		resMap["location"] = filter.Location
		queryParts = append(queryParts, `location IN @location`)
	}
	if filter.Sport != nil {
		resMap["sports"] = filter.Sport
		queryParts = append(queryParts, `sport IN @sports`)
	}
	if filter.IdLevel != nil {
		resMap["id_level"] = filter.IdLevel
		queryParts = append(queryParts, `id_level IN @id_level`)
	}
	query := strings.Join(queryParts, ` AND `)
	return query, resMap
}
