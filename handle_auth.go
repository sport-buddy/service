package main

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"net/http"
	"time"
)

var RefreshTokenExpiration = 30 * time.Hour * 24

const refreshToken = "refresh_token"
const xAuthToken = "X-Auth-Token"
const secret = "s0Wo!GLNLkjwVG4G:Jf18/KvAM"

func (s *serverImpl) loginHandler(c echo.Context) error {
	var authProps AuthProps
	err := c.Bind(&authProps)
	if err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, "invalid login parameters")
	}
	authProps.device = c.Request().UserAgent()
	resp, cookie, err := s.userManager.Authenticate(authProps)
	if err != nil {
		return c.String(http.StatusUnauthorized, err.Error())
	}
	c.SetCookie(cookie)
	return c.JSON(http.StatusOK, resp)
}

func (s *serverImpl) signupHandler(c echo.Context) error {
	var authProps AuthProps
	err := c.Bind(&authProps)
	if err != nil {
		log.Error(err.Error())
		return c.String(http.StatusBadRequest, "invalid signup parameters")
	}
	authProps.device = c.Request().UserAgent()
	resp, cookie, err := s.userManager.AddUser(authProps)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	c.SetCookie(cookie)
	return c.JSON(http.StatusOK, resp)
}

func (s *serverImpl) logoutHandler(c echo.Context) error {
	cookie, err := c.Cookie(refreshToken)
	if err != nil {
		err := fmt.Errorf("failed to get refresh token from cookie: %s", err.Error())
		log.Error(err.Error())
		return c.String(http.StatusUnauthorized, err.Error())
	}

	err = s.userManager.Logout(cookie.Value)
	if err != nil {
		err := fmt.Errorf("failed to logout: %s", err.Error())
		log.Error(err.Error())
		return err
	}
	cookie.MaxAge = -1
	c.SetCookie(cookie)
	return c.String(http.StatusOK, "Logout successfully")
}

func (s *serverImpl) refreshHandler(c echo.Context) error {
	cookie, err := c.Cookie(refreshToken)
	if err != nil {
		log.Error("failed to refresh token: ", err.Error())
		return c.String(http.StatusUnauthorized, err.Error())
	}
	device := c.Request().UserAgent()
	resp, newCookie, err := s.userManager.Refresh(cookie.Value, device)
	if err != nil {
		return err
	}
	c.SetCookie(newCookie)
	return c.JSON(http.StatusOK, resp)
}

func (s *serverImpl) accessMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token := c.Request().Header.Get(xAuthToken)
		if token == "" {
			token = c.QueryParam(xAuthToken)
		}
		tkn, err := jwt.ParseWithClaims(
			token,
			&jwt.StandardClaims{},
			func(token *jwt.Token) (interface{}, error) {
				return []byte(secret), nil
			},
		)
		if err != nil {
			log.Error("failed to parse token: ", err)
			return c.String(http.StatusUnauthorized, "failed to authenticate user: failed to parse token")
		}

		claims, ok := tkn.Claims.(*jwt.StandardClaims)

		if !ok {
			log.Error("failed to parse token: ", err)
			return c.String(http.StatusUnauthorized, "failed to parse token")
		}
		if claims.ExpiresAt < time.Now().UTC().Unix() {
			log.Error(errors.New("jwt expired"))
			return c.String(http.StatusUnauthorized, "JWT expired")
		}
		c.Set("id_user", claims.Id)
		return next(c)
	}
}
