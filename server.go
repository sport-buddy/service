package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"time"
)

type (
	serverImpl struct {
		serverApi       *echo.Echo
		port            string
		userManager     UserManager
		trainingManager TrainingManager
	}

	Server interface {
		Start()
		Stop()
		ServerApi() *echo.Echo
	}
)

func NewServer(port string, um UserManager, tm TrainingManager) Server {
	return &serverImpl{
		port:            port,
		userManager:     um,
		trainingManager: tm,
	}
}

// @title SB API
// @version 1.0
// @host localhost:3000
// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name X-Auth-Token
// @schemes: http
func (s *serverImpl) Start() {
	e := echo.New()

	e.Use(LogMiddleware)
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	//e.GET("/swagger/*", echoSwagger.WrapHandler)

	e.POST("/auth/login", s.loginHandler)
	e.POST("/auth/logout", s.logoutHandler, s.accessMiddleware)
	e.POST("/auth/signup", s.signupHandler)
	e.Any("/auth/refresh", s.refreshHandler)

	user := e.Group("/user")
	//user.DELETE("/:id", srv.handler.DeleteUserHandler, srv.handler.AccessMiddleware)
	user.GET("/:id/training", s.getUserTrainingsHandler, s.accessMiddleware)
	user.DELETE("/training/:id", s.removeUserFromTrainingHandler, s.accessMiddleware)
	//
	profile := user.Group("/profile")
	profile.GET("/:id", s.getUserProfileHandler, s.accessMiddleware)
	profile.PUT("", s.updateUserProfileHandler, s.accessMiddleware)
	//
	training := e.Group("/training")
	training.POST("/search", s.searchTrainingsHandler, s.accessMiddleware)
	training.POST("", s.addTrainingHandler, s.accessMiddleware)
	training.GET("/:id", s.getTrainingHandler, s.accessMiddleware)
	training.PUT("/:id", s.UpdateTrainingHandler, s.accessMiddleware)
	training.DELETE("/:id", s.deleteTrainingHandler, s.accessMiddleware)
	//
	msg := e.Group("/messenger")
	//msg.Any("", srv.handler.MessengerHandler, srv.handler.AccessMiddleware)
	//msg.POST("send", srv.handler.MessengerHandler, srv.handler.AccessMiddleware)
	//msg.GET("/dialogs", srv.handler.GetDialogsHandler, srv.handler.AccessMiddleware)
	//msg.POST("/dialogs/search", srv.handler.GetMessagesHandler, srv.handler.AccessMiddleware)
	msg.POST("/request", s.addUserToTraining, s.accessMiddleware)
	//msg.PUT("/request/reply", srv.handler.ReplyToRequestHandler, srv.handler.AccessMiddleware)
	//msg.PUT("/request/seen", srv.handler.DeclinedRequestSeenHandler, srv.handler.AccessMiddleware)

	s.serverApi = e
	startServer(e, s.port)
}

func (s *serverImpl) Stop() {
	s.serverApi.Close()
}

func (s *serverImpl) ServerApi() *echo.Echo {
	return s.serverApi
}

func startServer(e *echo.Echo, port string) {
	e.Logger.Fatal(e.Start(":" + port))
}

var (
	LogMiddleware = func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			start := time.Now()
			req := c.Request()
			log.Infof("%s %s", req.Method, req.URL)
			errorMsg := ""
			err := next(c)
			if err != nil {
				errorMsg = err.Error()
			}
			stop := time.Now()
			totalTime := stop.Sub(start)
			log.Infof("%s %s %d totalTime=%dms errors=%q", req.Method, req.URL, c.Response().Status, totalTime/time.Millisecond, errorMsg)
			return err
		}
	}
)
