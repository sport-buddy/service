module pet/sport-buddy

go 1.16

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/labstack/echo/v4 v4.7.2
	github.com/labstack/gommon v0.3.1
	github.com/markbates/pkger v0.17.1
	gorm.io/driver/postgres v1.3.5
	gorm.io/gorm v1.23.5
)
