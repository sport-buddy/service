--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';

--
-- Name: id_trigger(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.id_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
	new.username := new.id_user;
	return new;
end
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: group_training; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.group_training (
    id_training bigint DEFAULT (floor((random() * ((('9999999999'::bigint - 1000000000) + 1))::double precision)) + (1000000000)::double precision) NOT NULL,
    meet_date timestamp without time zone NOT NULL,
    location character varying(255) NOT NULL,
    id_sport integer NOT NULL,
    id_level integer DEFAULT 1 NOT NULL,
    fee integer DEFAULT 0,
    duration bigint,
    comment character varying(255)
);


--
-- Name: group_training_id_training_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE IF NOT EXISTS public.group_training_id_training_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: group_training_id_training_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE IF EXISTS public.group_training_id_training_seq OWNED BY public.group_training.id_training;


--
-- Name: levels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.levels (
    id_level serial PRIMARY KEY,
    level varchar(25) DEFAULT 'not_specified'::varchar
);


--
-- Name: locations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.locations (
    location character varying(50) NOT NULL,
    id_location serial NOT NULL
);

--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE IF NOT EXISTS public.locations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE if exists public.locations_id_seq OWNED BY public.locations.id_location;


--
-- Name: member_training; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.member_training (
    id_user bigint NOT NULL,
    id_training bigint NOT NULL,
    training_owner boolean DEFAULT false
);

--
-- Name: messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.messages (
    id_mes bigint DEFAULT (floor((random() * ((('9999999999'::bigint - 1000000000) + 1))::double precision)) + (1000000000)::double precision) NOT NULL,
    id_to bigint NOT NULL,
    id_from bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    content character varying
);


--
-- Name: person_locations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.person_locations (
    id_user bigint NOT NULL,
    id_location integer NOT NULL
);



--
-- Name: person_sports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.person_sports (
    id_user bigint NOT NULL,
    id_sport bigint NOT NULL
);


--
-- Name: relationships; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.relationships (
    id_from bigint NOT NULL,
    id_to bigint NOT NULL,
    seen boolean DEFAULT false,
    created_at timestamp without time zone,
    id_training bigint
);

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.schema_migrations (
    version bigint PRIMARY KEY,
    dirty boolean NOT NULL
);

--
-- Name: sessions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.sessions (
    id_user bigint NOT NULL,
    login_time timestamp without time zone NOT NULL,
    token character varying(255) NOT NULL,
    expires timestamp without time zone NOT NULL,
    device character varying
);

--
-- Name: sports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.sports (
    id_sport serial NOT NULL,
    sport_type character varying(255) NOT NULL
);

--
-- Name: user_auth_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.user_auth_info (
    id_user bigint NOT NULL,
    password character varying(255) NOT NULL
);

--
-- Name: user_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.user_info (
    id_user bigint NOT NULL,
    id_level integer DEFAULT 0,
    name character varying(50),
    second_name character varying(50),
    sex character varying(20),
    height integer,
    weight integer,
    email character varying(255),
    about character varying(255),
    birthday date
);

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE IF NOT EXISTS public.users (
    id_user bigint DEFAULT (floor((random() * ((('9999999999'::bigint - 1000000000) + 1))::double precision)) + (1000000000)::double precision) NOT NULL,
    username character varying(20) NOT NULL
);

--
-- Name: locations id_location; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.locations ALTER COLUMN id_location SET DEFAULT nextval('public.locations_id_seq'::regclass);


--
-- Data for Name: levels; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.levels (id_level, level) VALUES (0, 'not_specified'), (1, 'beginner'), (2, 'middle'), (3, 'senior') ON CONFLICT DO NOTHING;

--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.locations (location) VALUES ('Avtovo'), ('Nevskiy'), ('Chr'), ('Pioner'), ('Gorkovskaya');

--
-- Data for Name: sports; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sports (sport_type) VALUES ('tennis'),('football'),('athletics'),('running'),('volleyball'),('basketball'),
                                              ('swimming');
--
-- Data for Name: user_auth_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_auth_info (id_user, password) VALUES (0000000001, '$2a$08$M0AsXeq6Tx/OPF6pAcmPQ.xVaoMd1Yq9ae2MvVPKJhftECd7CSNOO') ON CONFLICT DO NOTHING;

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.users (id_user, username) VALUES (0000000001, 'admin') ON CONFLICT DO NOTHING;

--
-- Name: group_training_id_training_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.group_training_id_training_seq', 1, false);


--
-- Name: locations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.locations_id_seq', 4, true);


--
-- Name: group_training group_training_id_training_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_training
    ADD CONSTRAINT group_training_id_training_key UNIQUE (id_training);


--
-- Name: group_training group_training_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_training
    ADD CONSTRAINT group_training_pk PRIMARY KEY (id_training);


--
-- Name: group_training group_training_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_training
    ADD CONSTRAINT group_training_pkey UNIQUE (id_training, id_sport, id_level);

--
-- Name: locations locations_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pk PRIMARY KEY (id_location);


--
-- Name: messages messeges_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messeges_pkey PRIMARY KEY (id_mes);


--
-- Name: person_locations person_locations_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_locations
    ADD CONSTRAINT person_locations_pk UNIQUE (id_user, id_location);


--
-- Name: person_sports person_sports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_sports
    ADD CONSTRAINT person_sports_pkey PRIMARY KEY (id_user, id_sport);


--
-- Name: relationships relationships_id_user1_id_user2_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.relationships
    ADD CONSTRAINT relationships_id_user1_id_user2_key UNIQUE (id_from, id_to);



--
-- Name: sports sports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sports
    ADD CONSTRAINT sports_pkey PRIMARY KEY (id_sport);


--
-- Name: sports sports_sport_type_id_sport_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sports
    ADD CONSTRAINT sports_sport_type_id_sport_key UNIQUE (sport_type, id_sport);

--
-- Name: user_auth_info user_auth_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_auth_info
    ADD CONSTRAINT user_auth_info_pkey PRIMARY KEY (id_user);


--
-- Name: user_info user_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_info
    ADD CONSTRAINT user_info_pkey PRIMARY KEY (id_user);


--
-- Name: users user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT user_pkey PRIMARY KEY (id_user);


--
-- Name: locations_location_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX IF NOT EXISTS locations_location_uindex ON public.locations USING btree (location);


--
-- Name: member_training_id_user_id_training_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX IF NOT EXISTS member_training_id_user_id_training_uindex ON public.member_training USING btree (id_user, id_training);


--
-- Name: person_locations_id_user_id_location_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX IF NOT EXISTS person_locations_id_user_id_location_index ON public.person_locations USING btree (id_user, id_location);


--
-- Name: relationships_id_from_id_to_id_training_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX IF NOT EXISTS  relationships_id_from_id_to_id_training_index ON public.relationships USING btree (id_from, id_to, id_training);


--
-- Name: token; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX IF NOT EXISTS  token ON public.sessions USING btree (token);

--
-- Name: user_info_email_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX IF NOT EXISTS  user_info_email_uindex ON public.user_info USING btree (email);


--
-- Name: users_username_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX IF NOT EXISTS  users_username_uindex ON public.users USING btree (username);


--
-- Name: users id_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER id_trigger BEFORE INSERT OR UPDATE ON public.users FOR EACH ROW EXECUTE FUNCTION public.id_trigger();

ALTER TABLE public.users DISABLE TRIGGER id_trigger;


--
-- Name: member_training member_training_id_training_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_training
    ADD CONSTRAINT member_training_id_training_fkey FOREIGN KEY (id_training) REFERENCES public.group_training(id_training) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: member_training member_training_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_training
    ADD CONSTRAINT member_training_id_user_fkey FOREIGN KEY (id_user) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: messages messeges_id_from_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messeges_id_from_fkey FOREIGN KEY (id_from) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: messages messeges_id_to_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messeges_id_to_fkey FOREIGN KEY (id_to) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: person_locations person_locations_locations_id_location_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_locations
    ADD CONSTRAINT person_locations_locations_id_location_fk FOREIGN KEY (id_location) REFERENCES public.locations(id_location);


--
-- Name: person_locations person_locations_users_id_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_locations
    ADD CONSTRAINT person_locations_users_id_user_fk FOREIGN KEY (id_user) REFERENCES public.users(id_user);


--
-- Name: person_sports person_sports_id_sport_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_sports
    ADD CONSTRAINT person_sports_id_sport_fkey FOREIGN KEY (id_sport) REFERENCES public.sports(id_sport) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relationships relationships_id_user1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.relationships
    ADD CONSTRAINT relationships_id_user1_fkey FOREIGN KEY (id_from) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relationships relationships_id_user2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.relationships
    ADD CONSTRAINT relationships_id_user2_fkey FOREIGN KEY (id_to) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sessions sessions_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_id_user_fkey FOREIGN KEY (id_user) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_auth_info user_auth_info_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_auth_info
    ADD CONSTRAINT user_auth_info_id_user_fkey FOREIGN KEY (id_user) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_info user_info_id_level_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_info
    ADD CONSTRAINT user_info_id_level_fkey FOREIGN KEY (id_level) REFERENCES public.levels(id_level) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_info user_info_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_info
    ADD CONSTRAINT user_info_id_user_fkey FOREIGN KEY (id_user) REFERENCES public.users(id_user) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;

--
-- PostgreSQL database dump complete
--

